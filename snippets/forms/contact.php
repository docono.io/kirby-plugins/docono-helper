<form action="" id="contactform" method="post">
    <div>
        <label for="firstname"><?= t('form.firstname.label', 'Vorname') ?></label>
        <input type="text" name="firstname" id="firstname">

        <label for="lastname"><?= t('form.lastname.label', 'Nachname') ?></label>
        <input type="text" name="lastname" id="lastname">
    </div>
    <div>
        <label for="email"><?= t('form.email.label', 'E-Mail') ?></label>
        <input type="email" name="email" id="email">
    </div>
    <div>
        <label for="message"><?= t('form.message.label', 'Nachricht') ?></label>
        <textarea name="message" id="message"></textarea>
    </div>
    <button class="send-form"><?= t('form.send.btn', 'Absenden') ?></button>
</form>
