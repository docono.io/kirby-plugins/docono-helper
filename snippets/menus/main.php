<nav class="site-navigation" aria-hidden="true">
    <ul class="menu">
        <?php foreach (collection('mainmenu') as $navItem) : ?>
            <li class="menuItem <?php e($navItem->isActive(), 'active') ?>">
                <a href="<?= $navItem->url() ?>"><?= $navItem->title() ?></a>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>
<button class="hamburger" aria-hidden="false">
    <span></span>
    <span></span>
    <span></span>
</button>
