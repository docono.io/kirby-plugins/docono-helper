<?php
/**
 * Slider Block
 * @var \Kirby\Cms\Block $block ;
 */
?>
<?php if ($images = $block->images()->toFiles()): ?>
    <section class="slider full-width">
        <div class="scroller">
            <ul class="scroller__inner">
                <?php foreach ($images as $image): ?>
                <li>
                    <?= $image ?>
                </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </section>
<?php endif; ?>
