<?php
/**
 * Main Content Snippet
 * @var array $data ;
 */
$layouts = $data->main()->toLayouts();
?>
<?php foreach ($layouts as $layout) : ?>
    <?php
    $gridSize = $layout->attrs()->gridSize();
    $gridClass = $layout->attrs()->removeGrid() ;
    ?>
    <section <?php e($layout->removeGap()->toBool(false), 'data-nogap') ?> class="<?php e($gridClass->toBool() === false, 'grid ') ?> <?= $gridSize ?> <?= $layout->animation() ?>"
             style="--marginBlock:<?= $layout->marginBlock()->value() ?>em;">
        <?php foreach ($layout->columns() as $column) : ?>
            <div class="column" style="--colSpan:<?= $column->span() ?>;">
                <?php foreach ($column->blocks() as $block) : ?>
                    <div class="block block-type-<?= $block->type() ?>">
                        <?php snippet('blocks/' . $block->type(), ['block' => $block, 'layout' => $layout]) ?>
                    </div>
                <?php endforeach ?>
            </div>
        <?php endforeach ?>
    </section>
<?php endforeach ?>
