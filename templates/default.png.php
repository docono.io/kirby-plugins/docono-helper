<?php

/** @var Kirby\Cms\Page $page */

$canvas = imagecreatetruecolor(1200, 628);

$backgroundColor = imagecolorallocate($canvas, 255, 255, 255);
$titleColor = imagecolorallocate($canvas, 255, 255, 255);
$textColor = imagecolorallocate($canvas, 255, 255, 255);
$fontFile = './media/plugins/docono/docono-helper/fonts/OpenSans.ttf';

imagefill($canvas, 0, 0, $backgroundColor);

$backgroundFile = $page->image()->thumb(['width' => 1200, 'height' => 628, 'crop' => true])->url();

$backgroundImage = imagecreatefromjpeg($backgroundFile);
imagecopyresampled($canvas, $backgroundImage, 0, 0, 0, 0, imagesx($backgroundImage), imagesy($backgroundImage), imagesx($backgroundImage), imagesy($backgroundImage));

// $brandColor = imagecolorallocate($canvas, 66, 66, 66);
// imagefilledrectangle($canvas, 300, 0, 315, 628, $brandColor);

// $title  = $page->seotitle()->isNotEmpty() ? $page->seotitle()->toString() : $page->title()->toString();
// $title  = wordwrap($title, 40); // default value for third parameter $break = "\n"
// imagefttext($canvas, 50, 0, 150, 300, $titleColor, $fontFile, $title);

// $description = $page->seodescription()->isNotEmpty() ? $page->seodescription()->toString() : '';
// $description  = wordwrap($description, 60); // default value for third parameter $break = "\n"
// imagefttext($canvas, 24, 0, 150, 350, $textColor, $fontFile, $description);

$logoFile = './assets/img/hs-logo.png';
$logo = imagecreatefrompng($logoFile);
imagecopyresampled($canvas, $logo, 50, 50, 0, 0, imagesx($logo), imagesy($logo), imagesx($logo), imagesy($logo));

imagepng($canvas);
imagedestroy($canvas);
