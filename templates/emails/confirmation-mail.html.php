<?php

use Kirby\Cms\Html;
use Kirby\Toolkit\Str;

?>

<!doctype html>
<html lang="de" class="no-js">

<head>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600&display=swap">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css2?family=Merriweather:wght@700&display=swap">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style>
        body {
            font-family: "Open Sans", sans-serif;
            font-size: 18px;
            font-weight: 300;
            color: hsl(7, 26%, 25%);
            background-color: #c9c1b6;
        }

        a {
            color: inherit;
            text-decoration: underline;
            outline: none;
            line-height: 1.3;
        }

        p {
            margin: 0 0 20px;
        }

        b,
        strong {
            font-weight: 700;
        }

        ul {
            list-style: none;
            padding-left: 25px;
        }

        ul>li {
            position: relative;
        }

        .contact {
            width: 100%;
            max-width: 700px;
            margin: 0 auto;
            padding: 1em;
            background: #ffffff;
        }

        td {
            vertical-align: top;
        }

        hr {
            border-color: #c9c1b6;
            width: 30%;
            margin: 20px 0;
        }

        h1{
            display: inline-block;
            position: relative;
            padding-bottom: 1.25rem;
            color: inherit;
            font-size: 35px;
            font-weight: 700;
            font-family: "Merriweather", serif;
            letter-spacing: 0.05em;
            color: hsl(360, 79%, 55%);
        }

        h2 {
            margin: 0;
            font-size: 18px;
            font-weight: 700;
            text-transform: uppercase;
            letter-spacing: 0.05em;
            color: inherit;
            text-decoration: underline;
        }

        .email-footer {
            font-size: 16px;
            font-weight: 300;
        }
    </style>
</head>

<body>
    <table class="contact">
        <tr>
            <td>
                <table class="contact-content">
                    <tr>
                        <td width="700" style="max-width:100%;">
                            <table class="contact-content">
                                <tr>
                                    <td style="max-width:100%;">
                                        <table class="contact-content__row">
                                            <tr>
                                                <td>
                                                    <img src="<?= kirby()->url().'/assets/img/tls-small-logo.jpg'?>" alt="TLS Logo">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <h1 class="contact-content__title">
                                                        <?= Str::template($site->confirmationMailTitle()->kt(), [
                                                            'name' => $data['name'] ?? '',
                                                        ]) ?>
                                                    </h1>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="contact-content__row">

                                            <?= Str::template($site->confirmationMailText()->kt(), [
                                                'contact' => $data['contact'] ?? '',
                                                'name' => $data['name'] ?? '',
                                                'branch'   => $data['branch'] ?? '',
                                                'lawyer' => $data['lawyer'] ?? '',
                                                'message' => $data['message'] ?? '',
                                                'email' => $data['email'] ?? '',
                                                'telephone' => $data['telephone'] ?? ''
                                            ]) ?>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="700" style="max-width: 100%;" class="email-footer">
                            <hr>
                            <table width="100%" style="max-width: 100%;">
                                <tr>
                                    <td colspan="3" style="padding-bottom:1em;">
                                        <b>
                                            <?= $site->footer()->toObject()->name() ?>
                                        </b>
                                    </td>
                                </tr>
                                <tr>
                                    <?php foreach ($site->places()->toStructure() as $place) : ?>
                                        <td width="33%">
                                            <?= $place->address() ?><br>
                                            <?= $place->zip() ?>&nbsp;<?= $place->town() ?><br>
                                            <?= $place->phone() ?><br>
                                            <?= $place->email() ?>
                                            <?= Html::link($place->routelink(), 'Routenplaner', ['target' => '_blank']) ?>
                                        </td>
                                    <?php endforeach; ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>

</html>
