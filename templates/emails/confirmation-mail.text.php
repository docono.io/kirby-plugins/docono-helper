<?php

use Kirby\Toolkit\Str;

?>
<?= Str::template($site->confirmationMailTitle(), [
    "name" => $data["name"] ?? "",
]) ?>

<?= Str::template($site->confirmationMailText(), [
    "contact" => $data["contact"] ?? "",
    "branch"   => $data["branch"] ?? "",
    'name' => $data['name'] ?? '',
    "lawyer" => $data["lawyer"] ?? "",
    "message" => $data["message"] ?? "",
    "email" => $data["email"] ?? "",
    "telephone" => $data["telephone"] ?? ""
]) ?>

---

<?= $site->footer()->toObject()->name() ?>

<?php foreach ($site->places()->toStructure() as $place) : ?>
<?= nl2br($place->address() . "\n" . $place->zip() . " " . $place->town() . "\n" . $place->phone() . "\n" .  $place->mail(), false) ?>
<?php endforeach; ?>
