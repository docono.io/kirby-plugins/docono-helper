<?php

Kirby::plugin('docono/docono-helper', [

    /** Blueprints */
    'blueprints' => [
        /** Fields */
        'fields/main' => __DIR__ . "/blueprints/fields/main.yml",

        /** Blocks */
        'blocks/slider' => __DIR__ . "/blueprints/blocks/slider.yml",

        /** Files */
        'files/icon' => __DIR__ . "/blueprints/files/icon.yml",
        'files/default' => __DIR__ . "/blueprints/files/default.yml",

        /** Sections */
        'sections/children' => __DIR__ . "/blueprints/sections/children.yml",
        'sections/layoutcontent' => __DIR__ . "/blueprints/sections/layoutcontent.yml",

        /** Tabs */
        'tabs/pagefiles' => __DIR__ . "/blueprints/tabs/pagefiles.yml",
        'tabs/pagesettings' => __DIR__ . "/blueprints/tabs/pagesettings.yml",
        'tabs/emails' => __DIR__ . '/blueprints/tabs/emails.yml',


        /** Roles */
        'users/admin' => __DIR__ . '/blueprints/users/admin.yml',
        'users/editor' => __DIR__ . '/blueprints/users/editor.yml',
        'users/newseditor' => __DIR__ . '/blueprints/users/newseditor.yml',
        'users/team' => __DIR__ . '/blueprints/users/team.yml'
    ],

    /** Templates */
    'templates' => [
        /** OG Image */
        'default.png' => __DIR__ . '/templates/default.png.php',

        /** E-Mails */
        'emails/confirmation-mail.html' => __DIR__ . '/templates/emails/confirmation-mail.html.php',
        'emails/confirmation-mail.text' => __DIR__ . '/templates/emails/confirmation-mail.text.php',
        'emails/notification-mail.html' => __DIR__ . '/templates/emails/notification-mail.html.php',
        'emails/notification-mail.text' => __DIR__ . '/templates/emails/notification-mail.text.php',
    ],

    /** Snippets */
    'snippets' => [

        'forms/contact' => __DIR__ . '/snippets/forms/contact.php',
        'menus/main' => __DIR__ . '/snippets/menus/main.php',
        'content/main' => __DIR__ . '/snippets/content/main.php',
        'blocks/slider' => __DIR__ . '/snippets/blocks/slider.php',

        /** Blocks */
    ],

    /** Collections */
    'collections' => [
        'allpages' => require __DIR__ . '/collections/allpages.php',
        'mainmenu' => require __DIR__ . '/collections/mainmenu.php',
        'footermenu' => require __DIR__ . '/collections/footermenu.php',
    ]
]);
