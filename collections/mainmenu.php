<?php

use \Kirby\Cms\Site;

return function (Site $site) {
    return collection('allpages')->filterBy('InMenu', 'main');
};
